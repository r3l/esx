#! /usr/bin/python
# -*- coding: utf-8 -*-

# Vedere il file "config.py" per inserire la configurazione dei server da
# salvare

import config
import re
import paramiko
import subprocess
import datetime
import os


def sendMail(mail_from, mail_to, subject=None, text=None, files=None, server='127.0.0.1'):
    if type(mail_to) == list:
        mt = ''
        for a in mail_to:
           mt += ', ' if mt else ''
           mt += a
    else:
        mt = mail_to
    import smtplib
    from email.MIMEMultipart import MIMEMultipart
    from email.MIMEBase import MIMEBase
    from email.MIMEText import MIMEText
    from email import Encoders
    from os.path import basename
    msg = MIMEMultipart()
    msg['From'] = mail_from
    msg['To'] = mt
    msg['Subject'] = subject
    msg.attach(MIMEText(text))

    for f in files or []:
        part = MIMEBase('application', 'octet-stream')
        part.set_payload(open(f, 'rb').read())
        Encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="%s"' % basename(f) )
        msg.attach(part)

    smtp = smtplib.SMTP('smtp.gmail.com', 587)
    smtp.ehlo()
    smtp.starttls()
    smtp.ehlo
    smtp.login('admin@hbtsa.it', '.admhtaocbs1')
    smtp.sendmail(mail_from, mail_to, msg.as_string())
    smtp.close()


class Ebk(Exception):
    pass


class LOG(object):
    def __init__(self):
        super(LOG, self).__init__()
        self.logs = ''

    def add(self, message):
        dt = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        self.logs += '[{dt}] {message}\n'.format(dt=dt,message=message)

    def sep(self):
        self.add('='*80)

    def save(self, filename=None, data=None):
        fn = filename
        if not fn:
            dt = datetime.datetime.now().strftime('%Y%m%d%H%M')
            if data:
                data['dt'] = dt
                fn = data['logs'].format(**data)
            else:
                fn = '/tmp/backupESX.%s' % dt
        OF = open(fn, 'a')
        OF.write(self.logs)
        OF.close()



class Logged(object):
    def __init__(self, log=None):
        super (Logged, self).__init__()
        self.logs=log

    def log(self, message):
        if self.logs:
            self.logs.add(message)


class ESX(Logged):
    get_vm = '/usr/bin/vim-cmd vmsvc/getallvms'
    get_vmx = 'cat /vmfs/volumes/{ds}/{File}'
    make_snap = '/usr/bin/vim-cmd vmsvc/snapshot.create {ID} vcb_snap BK_{data}'
    remove_snap = '/usr/bin/vim-cmd vmsvc/snapshot.removeall {ID}'
    rm_old = 'rm -fr "/vmfs/volumes/{dsBackup}/{vmPath}"'
    mk_dir = 'mkdir -p "/vmfs/volumes/{dsBackup}/{vmPath}"'
    cp_vmx = 'cp "/vmfs/volumes/{ds}/{File}" "/vmfs/volumes/{dsBackup}/{File}"'
    clone_disk = 'vmkfstools -i "/vmfs/volumes/{ds}/{vmPath}/{disk}" "/vmfs/volumes/{dsBackup}/{vmPath}/{disk}"'
    zip_vm = ['tar', 'cfz', '{link_last}/{vmPath}.tgz', '{dsStorage}/{vmPath}']
    rm_zip_vm = '{link_last}/{vmPath}.tgz'
    ln = ['ln', '-s', '{rotazione}', '{link_last}']
    unlink = ['unlink', '{link_last}']
    lsln = ['readlink', '-f', '{link_last}']

    def __init__(self, data, log=None):
        super(ESX, self).__init__(log=log)
        self.server = data['ip']
        self.user = data['user']
        self.password = data['passwd']
        self.dsBackup = data['dsBackup']
        self.term = paramiko.SSHClient()
        self.login()

    def createRotation(self, data):
        self.log('Create rotation')
        Rot = data['rotation']
        try:
            last = self.execlocal(self.lsln, data).split()[0]
            lastid = Rot.index(last)
            self.execlocal(self.unlink, data)
        except:
            lastid = len(Rot)-1
            self.log('prima rotazione')
        if lastid == len(Rot)-1:
            lastid = 0
        else:
            lastid += 1
        data['rotazione'] = Rot[lastid]
        self.execlocal(self.ln, data)
        self.log('Rotazione su {rotazione} mappato su : {link_last}'.format(**data))

    def compressVM(self, data):
        self.log('Inizio compressione vm : {vmPath}'.format(**data))
        fn = self.rm_zip_vm.format(**data)
        if os.path.exists(fn):
            os.remove(fn)
        self.execlocal(self.zip_vm, data)
        self.log('Fine compressione vm : {vmPath}'.format(**data))

    def login(self):
        self.log('connecting to server {server} ...'.format(server=self.server))
        self.term.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.term.connect(self.server, username=self.user, password=self.password)
        self.log('login to server {server}.'.format(server=self.server))

    def logout(self):
        self.term.close()
        self.log('logout to server {server}.'.format(server=self.server))

    def getAllVM(self):
        try:
            self.log('get vms list ...')
            stI, stO, stE = self.term.exec_command(self.get_vm)
            testErr(stE)
        except Exception as e:
            raise Ebk('[getAllVM] Esecuzione comandi shell\n%s' % e)
        r = []

        for s in stO:
            pat = re.compile('''
                ^
                (?P<ID>\w+)
                \W+
                (?P<name>\S+)
                \W+
                \[(?P<ds>.*)\]
                \W+
                (?P<File>.*\.vmx)
                \W+
                (?P<OS>\w+)
                \W+
                (?P<Ver>\S+)
                \W*$
            ''', re.VERBOSE)
            res = pat.search(s)
            if res:
                res = res.groupdict()
                res['disks'] = self.getVMDK(res)
                self.log('vmID: {ID}, {ds}, {name}, {OS}'.format(**res))
                r.append(res)
        return r

    def execlocal(self, command, data):
        o = []
        for p in command:
            o.append (p.format(**data))
        #self.log('execlocal : >%s<' % o)
        return subprocess.check_output(o)

    def execmd(self, command):
        #self.log('execmd : >%s<' % command)
        stI, stO, stE = self.term.exec_command(command)
        for e in stE:
            self.log(e)
        return stO

    def getVMDK(self, data):
        try:
            disks = []
            command = self.get_vmx.format(**data)
            stI, stO, stE = self.term.exec_command(command)
            testErr(stE)
            for o in stO:
                pat = re.compile('^.*\"(?P<disk>\S+\.vmdk)\".*')
                res = pat.search(o)
                if res:
                    disks.append(res.groupdict())
            return disks
        except Exception as e:
            raise Ebk('[getVMDK][%s] %s' % (self.vm, e))

    def backupVM(self, data):
        data['vmPath'] = data['File'].split('/')[0]
        data['data'] = datetime.date.today().strftime('%Y%m%d')
        data['dsBackup'] = self.dsBackup
        try:
            try:
                self.log('remove old data on /{dsBackup}/{vmPath}'.format(**data))
                self.execmd(self.rm_old.format(**data))
            except:
                print 'primo backup'

            self.execmd(self.mk_dir.format(**data))

            self.log('copy config file for vm {name}'.format(**data))
            self.execmd(self.cp_vmx.format(**data))

            self.log('make snapshot {ID}, {name}'.format(**data))
            self.execmd(self.make_snap.format(**data))

            for d in data['disks']:
                self.log('cloning disk {disk}'.format(disk=d['disk'], **data))
                self.execmd(self.clone_disk.format(disk=d['disk'], **data))

            self.log('remove all snapshot {ID}, {name}'.format(**data))
            self.execmd(self.remove_snap.format(**data))
        except Exception as e:
            raise Ebk('[copyVM]\n[%s]\n %s' % (data, e))

    def storicoVM(self, data):
        data['vmPath'] = data['File'].split('/')[0]
        data['data'] = datetime.date.today().strftime('%Y%m%d')
        data['dsBackup'] = self.dsBackup
        try:
            self.execmd(self.make_snap.format(**data))
        except Exception as e:
            raise Ebk('[copyVM]\n[%s]\n %s' % (data, e))

def testErr(Err):
    err = ''
    for e in Err:
        err += "\n" + e
    if err:
        raise Ebk('[getAllVM] comandi shell con errori\n%s' % err)


def doBackup():
    vms = []
    logs = LOG()
    doRotation = False
    for srv in config.server:
        V = ESX(srv, log=logs)
        if not doRotation :
            V.createRotation(config.general)
            doRotation = True
        logs.sep()
        vm = V.getAllVM()
        logs.sep()
        for v in vm:
            # aggiungo o parametri di general
            logs.add('=== {name}'.format(**v))
            if ('filter' in srv) and (srv['filter']) :
                if v['name'] == srv['filter']:
                    V.backupVM(v)
            else:
                V.backupVM(v)
            vms.append(v)
            logs.sep()
        logs.sep()
        logs.sep()
        for v in vm:
            logs.add('=== {name}'.format(**v))
            # aggiungo o parametri di general
            for p in config.general:
                v[p] = config.general[p]
            if ('filter' in srv) and (srv['filter']) :
                if v['name'] == srv['filter']:
                    V.compressVM(v)
            else:
                V.compressVM(v)
            logs.sep()
    #print logs.logs
    M = config.general['mail']
    sendMail(M['from'], M['to'], 'Backup server', logs.logs)
    #print logs.logs
    logs.save(data=config.general)
    return vms


def main():
    try:
        info = doBackup()
        # for v in info:
        #     print v
    except Ebk as e:
        print 'Errore nella procedura di Backup'
        print e


main()
