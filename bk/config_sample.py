#
# -*- coding: utf-8 -*-

debug = True

serverHBTSA = [
    {
        'srvname': 'esx01.hbtsa.it',
        'ip': '192.168.70.11',
        'user': 'root',
        'passwd': '12345678',
        'dsBackup': 'dsBackup',
        'filter': None,
        #        'filter': 'XXXtorino.hbtsa.it',
    },
    {
        'srvname': 'esx02.hbtsa.it',
        'ip': '192.168.70.12',
        'user': 'root',
        'passwd': '12345678',
        'dsBackup': 'dsBackup',
        'filter': None,
    },
]


serverFAB42 = [
    {
        'srvname': 'esx01.fab42.it',
        'ip': '192.168.40.11',
        'user': 'root',
        'passwd': '12345678',
        'dsBackup': 'dsBackup',
        'filter': None,
        #        'filter': 'XXXtorino.hbtsa.it',
    },
]


server = serverHBTSA


general = {
    # parametrio per il mail server
    # per indirizzi multipli inel campo "to", inserire i valori in una lista
    'mail': {
        'to': ['backup@ezpro.it', 'info@hbtsa.it', 'test@lavind.it'],
        'from': 'bk01@hbtsa.it',
    },

    # directory del server dei backup in cui vengono salvate le vm (corrisponde
    # a dsBackup lato server ESXi)
    'dsStorage': '/storage/hd1',

    # Rotazioni
    'rotation': [
        '/storage/hd2/s1',
        '/storage/hd3/s2',
        '/storage/hd2/s3',
        '/storage/hd3/s4',
    ],
    # link all'ultimo backup (compresso), ovvero una delle rotazioni
    'link_last': '/storage/lastbackup',

    # logs
    # la variabile {dt} deve essere definita
    'logs': '/storage/logs/backup.{dt}.log',
}
