#! /usr/bin/python
# -*- coding: utf-8 -*-

# Vedere il file "config.py" per inserire la configurazione dei server da
# salvare

try:
    import config_sample as config
except:
    import config
import datetime
import paramiko
import re
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email import Encoders
from os.path import basename

DEBUG = config.debug


class EBackup(Exception):
    pass


class Log(object):
    '''
    classe per la gestione dei Log file
    '''

    def __init__(self, filename=None):
        super(Log, self).__init__()
        if filename:
            self.filename = filename
        else:
            fndt = datetime.datetime.now().strftime('%Y%m%d')
            self.filename = "backup_{dt}.log".format(dt=fndt)

    def add(self, message, level='I'):
        dt = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        s = '[{dt}][{level}] {message}\n'.format(
            dt=dt, message=message, level=level)
        with open(self.filename, 'a') as f:
            f.write(s)

    def error(self, message):
        self.add(message, 'E')

    def info(self, message):
        self.add(message)

    def sendMail(self, mail_from, mail_to, subject=None, text=None, files=None, server='127.0.0.1'):
        """
        funzione : sendMail
        invio del file di log via mail
        devono essere forniti tutti i dati per la connesione ad un server SMTP ed i destinatari
        parametri :
        data
        """
    # def sendMail(mail_from, mail_to, subject=None, text=None, files=None,
    # server='127.0.0.1'):
        if type(mail_to) == list:
            mt = ''
            for a in mail_to:
                mt += ', ' if mt else ''
                mt += a
        else:
            mt = mail_to
        msg = MIMEMultipart()
        msg['From'] = mail_from
        msg['To'] = mt
        msg['Subject'] = subject
        msg.attach(MIMEText(text))

        for f in files or []:
            part = MIMEBase('application', 'octet-stream')
            part.set_payload(open(f, 'rb').read())
            Encoders.encode_base64(part)
            part.add_header('Content-Disposition',
                            'attachment; filename="%s"' % basename(f))
            msg.attach(part)

        smtp = smtplib.SMTP('smtp.gmail.com', 587)
        smtp.ehlo()
        smtp.starttls()
        smtp.ehlo()
        smtp.login('admin@hbtsa.it', '.admhtaocbs1')
        smtp.sendmail(mail_from, mail_to, msg.as_string())
        smtp.close()

    def sep(self):
        self.add('=' * 80)

    def warning(self, message):
        self.add(message, 'W')


class Logged(object):
    '''
    classe base per gli oggetti con un log
    '''

    def __init__(self, log):
        super(Logged, self).__init__()
        self.Log = log

    def log(self, message):
        self.Log.add(message)

    def logDebug(self, message):
        if DEBUG:
            self.Log.add(message, "D")

    def logError(self, message):
        self.Log.error(message)

    def logInfo(self, message):
        self.Log.info(message)

    def logStart(self):
        self.Log.sep
        self.logInfo("Starting log ...")

    def logWarning(self, message):
        self.Log.warning(message)


class ESXi(Logged):
    '''
    Classe per la gestione del server ESXi
    vengono passati:
    data = dati di connessione al server ESXi,
    log = gestore del file log
    '''

    get_vm = '/usr/bin/vim-cmd vmsvc/getallvms'
    get_vmx = 'cat /vmfs/volumes/{ds}/{File}'
    make_snap = '/usr/bin/vim-cmd vmsvc/snapshot.create {ID} vcb_snap BK_{data}'
    remove_snap = '/usr/bin/vim-cmd vmsvc/snapshot.removeall {ID}'
    rm_old = 'rm -fr "/vmfs/volumes/{dsBackup}/{vmPath}"'
    mk_dir = 'mkdir -p "/vmfs/volumes/{dsBackup}/{vmPath}"'
    cp_vmx = 'cp "/vmfs/volumes/{ds}/{File}" "/vmfs/volumes/{dsBackup}/{File}"'
    clone_disk = 'vmkfstools -i "/vmfs/volumes/{ds}/{vmPath}/{disk}" "/vmfs/volumes/{dsBackup}/{vmPath}/{disk}"'
    zip_vm = ['tar', 'cfz', '{link_last}/{vmPath}.tgz', '{dsStorage}/{vmPath}']
    rm_zip_vm = '{link_last}/{vmPath}.tgz'
    ln = ['ln', '-s', '{rotazione}', '{link_last}']
    unlink = ['unlink', '{link_last}']
    lsln = ['readlink', '-f', '{link_last}']

    def __init__(self, data, log):
        super(ESXi, self).__init__(log)
        self.serverData = data
        self.term = paramiko.SSHClient()

    def execute(self, command):
        self.logDebug(command)
        stI, stO, stE = self.term.exec_command(command)
        err = ''
        for e in stE:
            self.logWarning(e)
            err += e + "\n"
        if err:
            err = command + err
            raise EBackup(err)
        return stO

    def getVMDK(self, data):
        """
        funzione : getVMDK
        ritorna la lista di dischi usata dalla vm
        parametri :
        vmID
        """
        disks = []
        pat = re.compile('^.*\"(?P<disk>\S+\.vmdk)\".*')
        vmdks = self.execute(self.get_vmx.format(**data))
        for vmdk in vmdks:
            res = pat.search(vmdk)
            if res:
                disks.append(res.groupdict())
        return disks

    def getVMList(self):
        """
        funzione : getVMList
        ritorna la lista completa delle VM disponibili nbel server ESXi
        """
        rlist = self.execute(self.get_vm)
        pat = re.compile('''
            ^
            (?P<ID>\w+)
            \W+
            (?P<name>\S+)
            \W+
            \[(?P<ds>.*)\]
            \W+
            (?P<File>.*\.vmx)
            \W+
            (?P<OS>\w+)
            \W+
            (?P<Ver>\S+)
            \W*$
        ''', re.VERBOSE)
        vmlist = []
        for vml in rlist:
            res = pat.search(vml)
            if res:
                res = res.groupdict()
                res['disks'] = self.getVMDK(res)
                res['serverData'] = self.serverData
                self.logInfo('vmID: {ID}, {ds}, {name}, {OS}'.format(**res))
                self.logDebug(res)
                vmlist.append(res)
        return vmlist

    def login(self):
        """
        funzione : login
        login al server ESXi
        """
        data = self.serverData
        for param in ("ip", "user", "passwd", "dsBackup"):
            if param not in data:
                raise EBackup(
                    "parameter [{param}] is missing".format(param=param))
        try:
            self.logInfo("login to server {ip}".format(**data))
            self.term.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            self.term.connect(data['ip'], username=data[
                              'user'], password=data['passwd'])
            self.logInfo("loged in to server {user}@{ip}".format(**data))
        except:
            raise EBackup(
                "unable to login to server {user}@{ip}".format(**data))

    def logout(self):
        """
        funzione : logout
        logout del server ESXi
        """
        self.term.close()
        self.logInfo(
            "logout from server {user}@{ip}".format(**self.serverData))


class Backup(Logged):
    """
    classe per la gestione dei backup
    """
    pass


if __name__ == "__main__":
    try:
        LOG = Log()
        vmlist = []
        for server in config.server:
            esx = ESXi(server, LOG)
            esx.login()
            vmlist += esx.getVMList()
            esx.logout()

        LOG.sep()
        for vm in vmlist:
            LOG.info(
                '|{serverData[srvname]:20}|{name:40}|{ID:5}|'.format(**vm))

    except EBackup as e:
        LOG.error('{}, {}'.format(type(e), e))
    except Exception as e:
        LOG.error('{}, {}'.format(type(e), e))
    finally:
        # LOG.sendMail()
        print "backup completo"
